Azure VM Hosted K3s
---

[AZ-204 Lab05 VMs](https://microsoftlearning.github.io/AZ-204-DevelopingSolutionsforMicrosoftAzure/Instructions/Labs/AZ-204_lab_05.html)


1. Install [k3sup](https://github.com/alexellis/k3sup#download-k3sup-tldr)
1. Create a Resource Group
```
source azureK3s.nocommit.env
az group create -l eastus -n ${AZURE_K3S_RESOURCE_GROUP}
```

1. Create Standard_D4ads_v5 8 vCPU 16GiB Ram 150 GiB Temporary Storage $0.206/hour pay as you go

```
az login
source azureK3s.nocommit.env
az vm create --resource-group ${AZURE_K3S_RESOURCE_GROUP} --name azureK3s --image ${AZURE_K3S_IMAGE} --size ${AZURE_K3S_SIZE} --admin-username ${AZURE_K3S_USER} --admin-password ${AZURE_K3S_PASSWORD} --generate-ssh-keys
az vm open-port --resource-group ${AZURE_K3S_RESOURCE_GROUP} --name azureK3s --port ${AZURE_K3S_PORT}
az vm open-port --resource-group ${AZURE_K3S_RESOURCE_GROUP} --name azureK3s --port 80 --priority 901
ipAddress=$(az vm list-ip-addresses --resource-group ${AZURE_K3S_RESOURCE_GROUP} --name azureK3s --query '[].{ip:virtualMachine.network.publicIpAddresses[0].ipAddress}' --output tsv)
ssh-copy-id -i ${KEY} ${AZURE_K3S_USER}@${ipAddress}
```

1. Install K3s with k3sup
```
k3sup install --ip ${ipAddress} --user ${AZURE_K3S_USER} --ssh-key $KEY
export KUBECONFIG=`pwd`/kubeconfig
k3s get node
kubectl create -f test-web-ingress.yaml
echo "${ipAddress} whoami.azurek3s.net" | sudo tee -a /etc/hosts
curl http://whoami.azurek3s.net
```

Clean Up
```
rm kubeconfig
az group delete --name ${AZURE_K3S_RESOURCE_GROUP} --no-wait --yes
---
